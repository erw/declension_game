#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
import random
from colorama import init, Fore, Back, Style
import Levenshtein
import readline  # noqa

from constants import Case, Number, \
    SIMPLIFY_MAP, NUMBER_COLOR_MAP, CASE_COLOR_MAP

init()


CASE_WEIGHTS = [
    (Case.NOM, Number.SING, 0), (Case.NOM, Number.PLUR, 1),
    (Case.GEN, Number.SING, 1), (Case.GEN, Number.PLUR, 0),
    (Case.DAT, Number.SING, 0), (Case.DAT, Number.PLUR, 0),
    (Case.ACC, Number.SING, 1), (Case.ACC, Number.PLUR, 0),
    (Case.INS, Number.SING, 0), (Case.INS, Number.PLUR, 0),
    (Case.LOC, Number.SING, 1), (Case.LOC, Number.PLUR, 0),
    (Case.VOC, Number.SING, 0), (Case.VOC, Number.PLUR, 0),
]

CASE_LIST = sum([[(case, number)] * weight
                 for case, number, weight
                 in CASE_WEIGHTS], [])


def parse_data():
    '''Pases the format copied from wiktionary declension tables'''
    words = []
    with open('declension_data.txt') as f:
        for line in f.readlines():
            line = line[:-1]  # strip newline
            if not line:
                continue

            first, sing, plur = map(lambda x: x.strip(), line.split('\t'))
            if first == '':
                word = []
                words.append(word)
            else:
                word.append((sing, plur))
    return words


def strip_stress_marks(string):
    for a, b in SIMPLIFY_MAP:
        string = string.replace(a, b)
    return string


def colored_case_name(case):
    return CASE_COLOR_MAP[case] + Case.name[case] + Style.RESET_ALL


def colored_number_name(number):
    return NUMBER_COLOR_MAP[number] + Number.name[number] + Style.RESET_ALL


def main():
    words = parse_data()
    random.shuffle(words)

    for word in words:

        question_case = Case.NOM
        question_number = Number.SING
        question_word = word[question_case][question_number]

        print('\n----------------------------------------'
              '\nThe word is:\n\n    {0} {1}  {2}\n\nPlease specify:\n'.format(
                  colored_case_name(question_case),
                  colored_number_name(question_number),
                  question_word,
              ))

        # TODO: Choose cases relevant for the current word, for example DUAL or
        # some words that are only used in PLUR or SING. Maybe intersect
        # CASE_LIST and what's available for the current word.

        case, number = random.choice(CASE_LIST)
        correct_answer = word[case][number]

        user_answer = raw_input('    {0} {1}  '.format(
            colored_case_name(case),
            colored_number_name(number),
        ))

        dist = Levenshtein.distance(strip_stress_marks(user_answer),
                                    strip_stress_marks(correct_answer))
        if dist == 0:
            status = Back.GREEN + 'Correct!'
        elif dist <= 2:
            status = Back.YELLOW + 'Almost there!'
        else:
            status = Back.RED + 'Sorry!'

        raw_input('\n              {0}  is the right answer. {1} '.format(
            correct_answer,
            Fore.WHITE + Style.BRIGHT + status + Style.RESET_ALL,
        ))


if __name__ == '__main__':
    try:
        main()
    except (KeyboardInterrupt, EOFError):
        pass
    print(Style.RESET_ALL + '\n\nIki!')
