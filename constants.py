#!/usr/bin/env python
# -*- coding: utf-8 -*-

from enum import enum
from colorama import Fore, Back

Case = enum(
    'NOM',
    'GEN',
    'DAT',
    'ACC',
    'INS',
    'LOC',
    'VOC',
)

Number = enum(
    'SING',
    #'DUAL',
    'PLUR',
)

CASE_COLOR_MAP = {
    Case.NOM: Fore.BLACK + Back.WHITE,
    Case.GEN: Fore.WHITE + Back.GREEN,
    Case.DAT: Fore.BLACK + Back.YELLOW,
    Case.ACC: Fore.BLACK + Back.RED,
    Case.INS: Fore.WHITE + Back.CYAN,
    Case.LOC: Fore.WHITE + Back.MAGENTA,
    Case.VOC: Fore.WHITE + Back.BLACK,
}

NUMBER_COLOR_MAP = {
    Number.SING: Fore.BLACK + Back.WHITE,
    Number.PLUR: Fore.WHITE + Back.BLUE,
}

SIMPLIFY_MAP = [
    ('ã', 'a'),
    ('ẽ', 'e'),
    ('ĩ', 'i'),
    ('õ', 'o'),
    ('ũ', 'u'),
    ('ỹ', 'y'),
    ('á', 'a'),
    ('à', 'a'),
    ('ì', 'i'),
    ('ù', 'u'),
    ('è', 'e'),
    ('ó', 'o'),

    ('ñ', 'n'),
    ('r̃', 'r'),
    ('ṽ', 'v'),
]
