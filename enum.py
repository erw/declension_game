#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Enum implementation from
# http://stackoverflow.com/questions/36932/how-can-i-represent-an-enum-in-python  # noqa
def enum(*sequential, **named):
    enums = dict(zip(sequential, range(len(sequential))), **named)
    reverse = dict((value, key) for key, value in enums.iteritems())
    enums['name'] = reverse
    return type('Enum', (), enums)
